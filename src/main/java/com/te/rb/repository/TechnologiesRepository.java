package com.te.rb.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.te.rb.entity.Technologies;

@Repository
public interface TechnologiesRepository extends JpaRepository<Technologies, String>{


}
