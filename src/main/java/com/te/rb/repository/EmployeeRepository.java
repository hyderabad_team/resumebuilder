package com.te.rb.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.te.rb.entity.Employee;

@Repository
public interface EmployeeRepository extends JpaRepository<Employee,String> {

}
