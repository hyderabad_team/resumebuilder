package com.te.rb.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.te.rb.entity.Roles;

@Repository
public interface RolesRepository extends JpaRepository<Roles, Integer>{

}
