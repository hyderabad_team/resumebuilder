package com.te.rb.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.te.rb.entity.Admin;

@Repository
public interface AdminRepository extends JpaRepository<Admin, String> {


}
