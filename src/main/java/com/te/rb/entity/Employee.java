package com.te.rb.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.OneToMany;

import com.google.common.collect.Lists;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
@Entity
public class Employee {

	@Id
	private String employeeId;

	private String employeeName;

	@OneToMany(mappedBy = "employee", cascade = CascadeType.ALL)
	private List<Resumes> resumes = Lists.newArrayList();

	@Lob
	private byte[] picture;

	@Lob
	private byte[] logo;

}
