package com.te.rb.entity;

import java.util.Arrays;
import java.util.List;

import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.checkerframework.common.aliasing.qual.Unique;

import com.sun.istack.NotNull;
import com.te.rb.util.ListToStringConverter;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
@Entity
public class Technologies {
	@Unique
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer technologyId;
	@Id
	private String technologyType;
	@Convert(converter = ListToStringConverter.class)
	private List<String>technologies=Arrays.asList();
}
