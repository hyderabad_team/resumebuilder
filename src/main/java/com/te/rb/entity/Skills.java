package com.te.rb.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

import com.google.common.collect.Lists;
import com.te.rb.util.ListToStringConverter;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Entity
public class Skills {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer skill_id;

	@Convert(converter = ListToStringConverter.class)
	private List<String> frontEndTechnologies = Lists.newArrayList();

	@Convert(converter = ListToStringConverter.class)
	private List<String> backEndTechnologies = Lists.newArrayList();

	@Convert(converter = ListToStringConverter.class)
	private List<String> middleWareTechnologies = Lists.newArrayList();

	@Convert(converter = ListToStringConverter.class)
	private List<String> designPatterns = Lists.newArrayList();

	@Convert(converter = ListToStringConverter.class)
	private List<String> databasesUsed = Lists.newArrayList();

	@Convert(converter = ListToStringConverter.class)
	private List<String> versionControlSystems = Lists.newArrayList();

	@Convert(converter = ListToStringConverter.class)
	private List<String> aws = Lists.newArrayList();

	@Convert(converter = ListToStringConverter.class)
	private List<String> sdlc = Lists.newArrayList();

	@OneToOne(cascade = CascadeType.ALL)
	private Resumes resumes;

}
