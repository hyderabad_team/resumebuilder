package com.te.rb.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import com.google.common.collect.Lists;
import com.te.rb.util.ListToStringConverter;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Entity
public class Resumes {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer resumeId;

	@OneToOne(mappedBy = "resumes", cascade = CascadeType.ALL)
	private Profile profile;

	@Convert(converter = ListToStringConverter.class)
	private List<String> summary;

	@OneToOne(mappedBy = "resumes", cascade = CascadeType.ALL)
	private Skills skills;

	@OneToOne(mappedBy = "resumes", cascade = CascadeType.ALL)
	private Education education;

	@OneToMany(mappedBy = "resumes",cascade = CascadeType.ALL)
	private List<ProjectDetails> projectDetails=Lists.newArrayList();
	
	@ManyToOne(cascade = CascadeType.ALL)
	private Employee employee;

}
