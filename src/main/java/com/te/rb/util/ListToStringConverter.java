package com.te.rb.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import com.google.common.base.Joiner;

@Converter(autoApply = true)
public class ListToStringConverter implements AttributeConverter<List<String>, String> {

	@Override
	public String convertToDatabaseColumn(List<String> attribute) {
		return Joiner.on(',').join(attribute);
	}

	@Override
	public ArrayList<String> convertToEntityAttribute(String dbData) {
		return new ArrayList(Arrays.asList(dbData.split(",")));
	}

}
