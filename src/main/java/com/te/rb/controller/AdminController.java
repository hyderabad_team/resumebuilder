package com.te.rb.controller;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.te.rb.dto.TechnologiesDto;
import com.te.rb.employee.service.AdminService;
import com.te.rb.response.GeneralResponse;

import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping(path = "/admin")
@RequiredArgsConstructor
public class AdminController {
	private final AdminService adminService;
	
	@PostMapping(path = "/addtechnologies")
	public GeneralResponse<String> addTechnologies(@RequestBody TechnologiesDto technologiesDto) {
		Boolean addTechnologies = adminService.addTechnologies(technologiesDto);
		if(addTechnologies) {
		return new GeneralResponse<String>("added Technologies successfully");
		}
		return new GeneralResponse<String>("unable to add technologies");
	}
}
