package com.te.rb.controller;

<<<<<<< HEAD
import java.util.List;
import java.util.Optional;

import org.springframework.web.bind.annotation.GetMapping;
=======
import java.io.IOException;
import java.util.Optional;

>>>>>>> 9d53847f5a799a779b0bc2825b06fab10c61eb84
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.te.rb.dto.NewResumeDto;
import com.te.rb.dto.RegistrationDto;
import com.te.rb.employee.service.EmployeeService;
import com.te.rb.exception.ReadTechnologiesFailedException;
import com.te.rb.exception.RegistrationFailedException;
import com.te.rb.exception.ResumeCreationFailedException;
import com.te.rb.response.GeneralResponse;

import lombok.RequiredArgsConstructor;

@RestController
@RequiredArgsConstructor
@RequestMapping(path = "/employee/{employeeId}")
public class EmployeeController {

	private final EmployeeService employeeService;

	@PostMapping(path = "/register")
	public GeneralResponse<RegistrationDto> employeeRegister(@RequestParam("picture") MultipartFile picture,
			@RequestParam("logo") MultipartFile logo, @RequestParam("userData") String userData) throws IOException {
		Optional<RegistrationDto> employee = employeeService.register(picture,logo,userData);
		if(employee.isPresent()) {
			return new GeneralResponse<RegistrationDto>("Employee Registered Successfully", employee.get());
		}
		throw new RegistrationFailedException("Employee Registration failed");
	}
<<<<<<< HEAD
	@GetMapping(path = "/gettechnologies/{technology}")
	public List<String> readTechnologies(@PathVariable("technology") String technology){
		Optional<List<String>>technologies=employeeService.readTechnologies(technology);
		if(technologies.isPresent()) {
			return technologies.get();
		}
		throw new ReadTechnologiesFailedException("unable to read the technologies..pass correct technology name") ;
		
	}
	
	
=======

	@PostMapping(path = "/createresume")
	public GeneralResponse<String> createResume(@RequestBody NewResumeDto newResumeDto,
			@PathVariable("employeeId") String employeeId) {
		Optional<Boolean> createResume = employeeService.createResume(newResumeDto, employeeId);
		if (createResume.get()) {
			return new GeneralResponse<String>("new resume has been created");
		}
		throw new ResumeCreationFailedException("unable to create resume");

	}

>>>>>>> 9d53847f5a799a779b0bc2825b06fab10c61eb84
}
