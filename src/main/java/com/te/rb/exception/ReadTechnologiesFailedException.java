package com.te.rb.exception;

public class ReadTechnologiesFailedException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public ReadTechnologiesFailedException(String message) {
		super(message);
	}


}
