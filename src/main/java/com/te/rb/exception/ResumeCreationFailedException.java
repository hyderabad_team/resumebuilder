package com.te.rb.exception;

public class ResumeCreationFailedException extends RuntimeException {

	public ResumeCreationFailedException(String message) {
		super(message);
	}

}
