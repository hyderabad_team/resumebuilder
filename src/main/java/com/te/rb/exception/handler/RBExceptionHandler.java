package com.te.rb.exception.handler;

import java.util.Map;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import com.google.common.collect.Maps;
import com.te.rb.exception.ReadTechnologiesFailedException;
import com.te.rb.exception.RegistrationFailedException;
import com.te.rb.exception.ResumeCreationFailedException;

@RestControllerAdvice
public class RBExceptionHandler {

	@ResponseStatus(value = HttpStatus.BAD_REQUEST)
	@ExceptionHandler(RegistrationFailedException.class)
	public Map<String, String> handler(RegistrationFailedException ex) {
		Map<String, String> map = Maps.newHashMap();
		map.put("message", ex.getMessage());
		return map;
	}
<<<<<<< HEAD
	@ResponseStatus(value = HttpStatus.BAD_REQUEST)
	@ExceptionHandler(ReadTechnologiesFailedException.class)
	public Map<String, String> handler(ReadTechnologiesFailedException ex) {
=======

	@ResponseStatus(value = HttpStatus.BAD_REQUEST)
	@ExceptionHandler(ResumeCreationFailedException.class)
	public Map<String, String> handler(ResumeCreationFailedException ex) {
>>>>>>> 9d53847f5a799a779b0bc2825b06fab10c61eb84
		Map<String, String> map = Maps.newHashMap();
		map.put("message", ex.getMessage());
		return map;
	}
}
