package com.te.rb.dto;

import java.util.List;

import com.google.common.collect.Lists;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
public class NewResumeDto {

	private ProfileDto profile;

	private List<String> summary = Lists.newArrayList();

	private SkillsDto skills;

	private EducationDto education;

	private List<ProjectDetailsDto> projectDetails = Lists.newArrayList();

}
