package com.te.rb.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
public class ProfileDto {

	private String firstName;

	private String lastName;

	private String technology;

	private Double totalExperience;

	private Double relevantExperience;
}
