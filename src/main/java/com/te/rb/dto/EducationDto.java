package com.te.rb.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
public class EducationDto {

	private String highestEducation;

	private String specialization;

	private String university;

	private Integer passoutYear;

	private Double percentage;
}
