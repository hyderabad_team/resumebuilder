package com.te.rb.dto;

import java.util.List;

import com.google.common.collect.Lists;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
public class SkillsDto {

	private List<String> frontEndTechnologies = Lists.newArrayList();

	private List<String> backEndTechnologies = Lists.newArrayList();

	private List<String> middleWareTechnologies = Lists.newArrayList();

	private List<String> designPatterns = Lists.newArrayList();

	private List<String> databasesUsed = Lists.newArrayList();

	private List<String> versionControlSystems = Lists.newArrayList();

	private List<String> aws = Lists.newArrayList();

	private List<String> sdlc = Lists.newArrayList();

}
