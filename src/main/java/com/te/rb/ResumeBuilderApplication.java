package com.te.rb;

import org.modelmapper.ModelMapper;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import com.google.common.collect.Lists;
import com.te.rb.entity.Admin;
import com.te.rb.entity.AppUser;
import com.te.rb.entity.Roles;
import com.te.rb.repository.AdminRepository;
import com.te.rb.repository.AppUserRepository;
import com.te.rb.repository.RolesRepository;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@SpringBootApplication
public class ResumeBuilderApplication {

	private final RolesRepository rolesRepository;
	private final AdminRepository adminRepository;
	private final AppUserRepository appUserRepository;
	
	@Bean
	public ModelMapper modelMapper()
	{
		return new ModelMapper();
	}
	
	public static void main(String[] args) {
		SpringApplication.run(ResumeBuilderApplication.class, args);
	}

	@Bean
	public CommandLineRunner runner() {
		return args -> {
			Roles admin = Roles.builder().roleName("ROLE_ADMIN").appUsers(Lists.newArrayList()).build();
			Roles employee = Roles.builder().roleName("ROLE_EMPLOYEE").appUsers(Lists.newArrayList()).build();
			Admin admin01 = Admin.builder().adminId("Admin01").adminName("Admin").build();
			AppUser adminCredentials = AppUser.builder().username(admin01.getAdminName()).password("qwerty")
					.roles(Lists.newArrayList()).build();
			adminCredentials.getRoles().add(admin);
			admin.getAppUsers().add(adminCredentials);
			rolesRepository.save(employee);
			rolesRepository.save(admin);
			adminRepository.save(admin01);
			appUserRepository.save(adminCredentials);
		};
	}

}
