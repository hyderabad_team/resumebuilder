package com.te.rb.employee.service.implementation;

import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import com.te.rb.dto.TechnologiesDto;
import com.te.rb.employee.service.AdminService;
import com.te.rb.entity.Technologies;
import com.te.rb.repository.AdminRepository;
import com.te.rb.repository.TechnologiesRepository;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class AdminServiceImpl implements AdminService {
	private final TechnologiesRepository technologiesRepository;
	@Override
	public Boolean addTechnologies(TechnologiesDto technologiesDto) {
		Technologies technologies=new Technologies();
		BeanUtils.copyProperties(technologiesDto, technologies);
		technologiesRepository.save(technologies);
		return true;
	}

}
