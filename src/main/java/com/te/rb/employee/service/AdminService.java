package com.te.rb.employee.service;

import com.te.rb.dto.TechnologiesDto;

public interface AdminService {

	Boolean addTechnologies(TechnologiesDto technologiesDto);

}
