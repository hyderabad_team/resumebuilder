package com.te.rb.employee.service.implementation;

<<<<<<< HEAD
import java.util.List;
=======
import java.io.IOException;
>>>>>>> 9d53847f5a799a779b0bc2825b06fab10c61eb84
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Lists;
import com.te.rb.dto.NewResumeDto;
import com.te.rb.dto.ProjectDetailsDto;
import com.te.rb.dto.RegistrationDto;
import com.te.rb.employee.service.EmployeeService;
import com.te.rb.entity.AppUser;
<<<<<<< HEAD
import com.te.rb.entity.Technologies;
import com.te.rb.repository.AppUserRepository;
import com.te.rb.repository.TechnologiesRepository;
=======
import com.te.rb.entity.Education;
import com.te.rb.entity.Employee;
import com.te.rb.entity.Profile;
import com.te.rb.entity.ProjectDetails;
import com.te.rb.entity.Resumes;
import com.te.rb.entity.Skills;
import com.te.rb.repository.AppUserRepository;
import com.te.rb.repository.EmployeeRepository;
>>>>>>> 9d53847f5a799a779b0bc2825b06fab10c61eb84

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class EmployeeServiceImpl implements EmployeeService {

	private final EmployeeRepository employeeRepository;

	private final AppUserRepository appUserRepository;
<<<<<<< HEAD
	
	private final TechnologiesRepository technologiesRepository;
	
=======

	private final ObjectMapper objectMapper;

	private final ModelMapper modelMapper;

>>>>>>> 9d53847f5a799a779b0bc2825b06fab10c61eb84
	@Override
	public Optional<RegistrationDto> register(MultipartFile picture, MultipartFile logo, String userData)
			throws IOException {
		Employee employee = new Employee();
		RegistrationDto readValue = null;
		readValue = mapper.readValue(userData, RegistrationDto.class);
		employee.setPicture(picture.getBytes());
		employee.setLogo(logo.getBytes());
		employee.setEmployeeId(readValue.getEmployeeId());
		employee.setEmployeeName(readValue.getEmployeeName());
		AppUser appUser = new AppUser();
		appUser.setUsername(readValue.getEmployeeId());
		appUser.setPassword(readValue.getPassword());
		appUserRepository.save(appUser);
		employeeRepository.save(employee);
		return Optional.ofNullable(readValue);
	}

	@Override
	public Optional<Boolean> createResume(NewResumeDto newResumeDto, String employeeId) {
		Optional<Employee> employee = employeeRepository.findById(employeeId);
		if (employee.isPresent()) {
			Resumes resumes = modelMapper.map(newResumeDto, Resumes.class);
			Education education = modelMapper.map(newResumeDto.getEducation(), Education.class);
			Profile profile = modelMapper.map(newResumeDto.getProfile(), Profile.class);

			List<ProjectDetails> projectDetails = newResumeDto.getProjectDetails().stream()
					.map(e -> modelMapper.map(e, ProjectDetails.class)).collect(Collectors.toList());
			Skills skills = modelMapper.map(newResumeDto.getSkills(), Skills.class);
			skills.setResumes(resumes);
			profile.setResumes(resumes);
			skills.setResumes(resumes);
			education.setResumes(resumes);
			projectDetails.forEach(System.out::println);
			projectDetails.stream().forEach(e -> e.setResumes(resumes));
			resumes.setProjectDetails(projectDetails);
			resumes.setEducation(education);
			resumes.setEmployee(employee.get());
			resumes.setProfile(profile);
			resumes.setSkills(skills);
			employee.get().getResumes().add(resumes);
		    employeeRepository.save(employee.get());
			return Optional.ofNullable(true);
		}

		return Optional.ofNullable(false);

	}

	@Override
	public Optional<List<String>> readTechnologies(String technology) {
		Optional<Technologies> fromDb = technologiesRepository.findById(technology);
		if(fromDb.isPresent()) {
		return Optional.ofNullable(fromDb.get().getTechnologies());
		}
		return Optional.ofNullable(null);
	}

}
