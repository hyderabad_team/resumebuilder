package com.te.rb.employee.service;

<<<<<<< HEAD
import java.util.List;
=======
import java.io.IOException;
>>>>>>> 9d53847f5a799a779b0bc2825b06fab10c61eb84
import java.util.Optional;

import org.springframework.web.multipart.MultipartFile;

import com.te.rb.dto.RegistrationDto;

public interface EmployeeService {

	Optional<RegistrationDto> register(MultipartFile picture, MultipartFile logo, String userData) throws IOException;

	Optional<Boolean> createResume(NewResumeDto newResumeDto, String employeeId);

	Optional<List<String>> readTechnologies(String technology);

}
